package JAVA;

import java.util.*;

public class Node {
    public Node sx;
    public Node dx;
    public ArrayList<Node> adiac;
    public int value;
    public Node(Node sx , Node dx, int value){
        this.sx = sx;
        this.dx = dx;
        this.value = value;
    }

    public Node(int value){
        this.value = value;
        this.adiac = new ArrayList<>();
    }

    public static ArrayList<Node> Ampiezza(Node node){
        Queue<Node> tmp = new LinkedList<Node>();
        ArrayList<Node> list = new ArrayList<Node>();

        if(node != null) tmp.add(node);
        while (!tmp.isEmpty()){
            Node i = tmp.poll();
            list.add(i);
            for (int j = 0; j < i.adiac.size(); j++){
                if(!list.contains(i.adiac.get(j)) && !tmp.contains(i.adiac.get(j)))
                    tmp.add(i.adiac.get(j));
            }
        }
        return list;
    }

    public static ArrayList<Node> Profondita(Node node){
        Stack<Node> tmp = new Stack<Node>();
        ArrayList<Node> list = new ArrayList<Node>();

        if(node != null) tmp.push(node);
        while (!tmp.isEmpty()){
            Node i = tmp.pop();
            list.add(i);
            for (int j = 0; j < i.adiac.size(); j++){
                if(!list.contains(i.adiac.get(j)) && !tmp.contains(i.adiac.get(j)))
                    tmp.push(i.adiac.get(j));
            }
        }
        return list;
    }

    public static String PrintNodeList(ArrayList<Node> list){
        String str = "";
        for (int i = 0; i < list.size(); i++){
            str = str + list.get(i).value + ",";
        }
        return str;
    }


    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        Node n6 = new Node(6);
        Node n7 = new Node(7);

        n1.adiac.add(n2);
        n1.adiac.add(n3);
        n1.adiac.add(n7);
        n2.adiac.add(n4);
        n2.adiac.add(n5);
        n2.adiac.add(n6);
        n3.adiac.add(n2);
        n3.adiac.add(n7);
        n3.adiac.add(n5);
        n6.adiac.add(n1);

        ArrayList<Node> l1 = Node.Ampiezza(n1);
        ArrayList<Node> l2 = Node.Profondita(n1);

        System.out.println("Ampiezza: " + Node.PrintNodeList(l1));
        System.out.println("Profondità: " + Node.PrintNodeList(l2));
    }

}
