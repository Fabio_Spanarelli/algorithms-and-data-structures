package JAVA;
import java.util.*;

public class Graph {
    private Map<Integer, List<Integer>> adiacList;
    private int[][] adiacMatrix;
    private int N_node;
    private int currentNodes = 0;
    public enum GraphType{
        ORIENTED,
        NON_ORIENTED
    }
    private GraphType graphType;

    public Graph(int N_node, GraphType graphType) {
        this.graphType = graphType;
        this.N_node = N_node;
        this.adiacList = new HashMap<>();
        this.adiacMatrix = new int[N_node][N_node];

        for (int i = 0; i < N_node; i++){
            for (int j = 0; j < N_node; j++){
                adiacMatrix[i][j] = 0;
            }
        }
    }

    public void addNode(int node) {
        this.currentNodes = this.currentNodes++;
        if (this.currentNodes > this.N_node){
            System.out.println("Node limit reached");
        }else{
            adiacList.put(node, new ArrayList<>());
        }
    }

    public void addArch(int node1, int node2) {
        if(node1 == node2 || this.graphType == GraphType.ORIENTED){
            adiacList.get(node1).add(node2);
            adiacMatrix[node1][node2] = 1;
        }else {
            adiacList.get(node1).add(node2);
            adiacList.get(node2).add(node1);
            adiacMatrix[node1][node2] = 1;
            adiacMatrix[node2][node1] = 1;
        }
    }

    public List<Integer> ottieniAdiacenti(int vertice) {
        return adiacList.get(vertice);
    }

    public void stampaMatriceAdiacenza() {
        for (int i = 0; i < N_node; i++) {
            System.out.println(Arrays.toString(adiacMatrix[i]));
        }
    }

    public static void main(String[] args) {
        Graph grafo = new Graph(4, GraphType.ORIENTED);

        grafo.addNode(0);
        grafo.addNode(1);
        grafo.addNode(2);
        grafo.addNode(3);

        grafo.addArch(1, 1);
        grafo.addArch(1, 0);
        grafo.addArch(1, 2);
        grafo.addArch(3, 3);

        List<Integer> adiacentiDiUno = grafo.ottieniAdiacenti(1);
        List<Integer> adiacentiDiDue = grafo.ottieniAdiacenti(2);
        System.out.println("Vertici adiacenti a 1: " + adiacentiDiUno);
        System.out.println("Vertici adiacenti a 2: " + adiacentiDiDue);
        grafo.stampaMatriceAdiacenza();
    }

}


