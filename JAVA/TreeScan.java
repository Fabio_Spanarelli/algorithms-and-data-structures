package JAVA;

import java.util.*;

public class TreeScan {
    public static List<Integer> Profond( Node root ){
        Stack<Node> tmp = new Stack<Node>();
        List<Integer> visitp = new ArrayList<Integer>();
        if(root != null) tmp.push(root);

        while (!tmp.isEmpty()) {
            Node current = tmp.pop();
            visitp.add(current.value);
            if(current.sx != null) {
                tmp.push(current.sx);
            }
            if (current.dx != null) {
                tmp.push(current.dx);
            }
        }
        return visitp;
    }

    public static List<Integer> Ampiez(Node root ){
        Queue<Node> tmp = new LinkedList<Node>();
        List<Integer> visitp = new ArrayList<Integer>();
        if(root != null) tmp.add(root);

        while (!tmp.isEmpty()) {
            Node current = tmp.poll();
            visitp.add(current.value);
            if(current.dx != null) {
                tmp.add(current.dx);
            }
            if (current.sx != null) {
                tmp.add(current.sx);
            }
        }
        return visitp;
    }


    public static void main(String[] args){
        Node sette = new Node(null,null, 7);
        Node sei = new Node(null,null, 6);
        Node cinque = new Node(null,null, 5);
        Node quattro = new Node(null,null, 4);
        Node due = new Node(cinque,quattro, 2);
        Node tre = new Node(sette,sei, 3);
        Node root = new Node(tre,due, 1);

        List<Integer> vect = TreeScan.Profond(root);
        List<Integer> vect2 = TreeScan.Ampiez(root);

        System.out.println("Tree Prof: " + vect);
        System.out.println("Tree Amp: " + vect2);

    }

}

